﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ImprovChallenge.UnitTest
{
    [TestClass]
    public class SecondHighestValueTest
    {
        [TestMethod]
        public void CanGetSecondHighestValue()
        {
            //Arrange
            int[] array = { 5, 6, 4, 2, 1 };

            //Act
            int secondHighest = Exam.SecondHighestValue(array);

            //Assert
            Assert.IsTrue(secondHighest == 5);

       }
    }
}
