﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ImprovChallenge.UnitTest
{
    [TestClass]
    public class RemoveDuplicateCharactersTest
    {
        [TestMethod]
        public void CanRemoveDuplicateCharacters()
        {
            //Arrange
            string test = "aabcdeef";

            //Act
            string noDupes = Exam.RemoveDuplicateCharacters(test);

            //Assert
            Assert.IsTrue(noDupes == "abcdef");
        }
    }
}
