﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ImprovChallenge.UnitTest
{
    [TestClass]
    public class ReverseCharactersTest
    {
        [TestMethod]
        public void CanGetReveresedString() {
            //Arrange
            string test = "abcd";

            //Act
            string reverse = Exam.ReverseCharacters(test);

            //Assert
            Assert.IsTrue(reverse == "dcba");
        }

    }
}
