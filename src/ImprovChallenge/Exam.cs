﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImprovChallenge
{
    public class Exam
    {
        public static int SecondHighestValue(int[] values) {

            int highest = 0;
            int secondHighest= 0;

            for (int i = 0; i < values.Length; i++) {
                int item = values[i];

                if (item > highest)
                {
                    secondHighest = highest;  
                    highest = item;
                }
                else if (item > secondHighest && item < highest) 
                    secondHighest = item;
            }

            return secondHighest;
        }

        public static string ReverseCharacters(string value) {

            string result = "";

            for (int i = value.Length - 1; i >= 0; i--) {
                result = result + value[i];
            }

            return result;
        }
        public static string RemoveDuplicateCharacters(string value) {
            string result = "";

            for (int i = 0; i < value.Length; i++)
            {
                char item = value[i];
                if (result.IndexOf(item) == -1) {
                    result += item;
                }
            
            }
            return result;
        } 

    }
}
